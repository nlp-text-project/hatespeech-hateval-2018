"""
    Preprocessing.py

    contiene todos las funciones que se utilizarán para un preprocesamiento
    de los texto. Esto incluye:
        - Normalizacion de texto.
        - Transformacion de texto aun formato dado
        - Eliminar caracteres raros

    Contiene los metodos:
        - GenerarBigramas
        - ConvertirListaToken
        - EliminarCaracteresIndeseados
        - EliminarStopWords
        - ConvertirMinuscula
        - NormalizarUsuario
        - NormalizarHTTP
        - NormalizarHashTag
        - NormalizarNumeros
"""
import re
from collections import Counter


def ConvertirListaToken(data_text):
    """
    Convierte una lista de string a una lista de lista de tokens
    :param data_text: lista de strings, texto a convertir
    :return: lista de lista de tokens
    """
    rpta =[]
    for texto in data_text:
        rpta.append( texto.split())

    return rpta


def ConvertirListaTokenDataFrame( pandas_datos, columna ):
    """
    Convierte una columna de pandas en un lista de lista de token

    :param pandas_datos: Pandas, Archivo con los datos
    :param columna: String, nombre de la columna de pandas_datos
    :return: Lista de lista de string, lista de listas de tokens
    """
    rpta = []
    columna_datos = pandas_datos.loc[ : , columna ]
    for text in columna_datos:
        rpta.append(text.split())
    return rpta


def GenerarBigramas(datos, ventana=1, llenado=False):
    """
    Generar bigramas con distancia 'ventana'

    :param datos: lista de lista de string; las oraciones del texto
    :return: un diccionario con bigramas y la cantidad de bigramas en total
    """
    rpta = Counter()
    cantidad_bigramas = 0
    if llenado:
       ventana = [ idx for idx in range(1, ventana+1)]
    elif not llenado:
        ventana = [ventana]

    for texto in datos:
        for indice in range(len(texto)):
            for avance in ventana:
                if indice + avance < len(texto):
                    rpta[(texto[indice],texto[indice + avance])] += 1
                    cantidad_bigramas += 1
                else:
                    break

    return {'cantidad': cantidad_bigramas, 'bigramas': rpta}


def EliminarCaracteresIndeseados(datos, tokens):
    """
    Eliminar todos los caracteres indeseados de el string

    :param datos: lista de string, textos a analizar
    :param tokens: lista de string, tokens que seran borrados de
    :return: Lista de string sin caracteres deseados
    """
    rpta = []
    for texto in datos:
        for token in tokens:
            texto = texto.replace(token, ' ')
        rpta.append(texto)
    return rpta


def EliminarStopWords(datos, stopword_list):
    """
    Eliminar stop words de los texto

    :param datos: lista de string, texto a analizar
    :param stopword_list: lista de string, lista de stop words
    :return: tener una texto sin stop words
    """
    rpta = []
    for texto in datos:
        new_texto = []
        for palabras in texto.split():
            if palabras not in stopword_list:
                new_texto.append(palabras)
        rpta.append(" ".join(new_texto))

    return rpta


def ConvertirMinuscula(datos):
    """
    Convertir todo a minuscula

    :param datos: lista de string, estructura de los datos en minuscula
    :return: texto en minuscula
    """
    rpta = []
    for texto in datos:
        rpta.append( texto.lower())

    return rpta


def NormalizarUsuario(datos):
    """
    Normalizador de usuarios de twitter con etiquetas especificas

    :param datos: lista de string, texto a analizar
    :return: Tener un normalizar de usuarios con una etiqueta especifica
    """
    rpta = []
    regexpress = r'\(?@[\d\w|\-|\_]+\)?\:?'
    for texto in datos:
        texto_rpta = re.sub(regexpress, ' USUARIO ', texto)
        rpta.append(texto_rpta)

    return rpta


def NormalizarHTTP(datos):
    """
    Normalizador los https de twitter con etiquetas especificas

    :param datos: lista de string, texto a amalizar
    :return: Texto con web normalizados usando etiquetas especificas
    """
    rpta = []
    regexpress = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|''[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    for texto in datos:
        texto_processado = re.sub(regexpress, ' WEB ', texto)
        rpta.append(texto_processado)

    return rpta


def NormalizarHashTag(datos):
    """
    Normalizador los HashTag de twitter con etiquetas especificas

    :param datos: lista de string, texto a amalizar
    :return: Texto con HashTag normalizados usando etiquetas especificas
    """
    rpta = []
    regexpress = r'#[\d\w|\-|\_]+'
    for texto in datos:
        texto_processado = re.sub(regexpress, ' HASHTAG ', texto)
        rpta.append(texto_processado)

    return rpta


def NormalizarNumeros(datos):
    """
    Normalizador los numeros de twitter con etiquetas especificas

    :param datos: lista de string, texto a amalizar
    :return: Texto con numeros normalizados usando etiquetas especificas
    """
    rpta = []
    regexpress = r' [-+]?[.\d]*[\d]+[:,.\d]* '
    for texto in datos:
        texto_processado = re.sub(regexpress, ' NUMERO ', texto)
        rpta.append(texto_processado)

    return rpta


def EliminacionUsandoRegex(datos, regex):
    """
    Elimina palabras indeseadas usando una expresion regular
    :param datos: lista de string, cada texto a analizar
    :param regex: string, expresion regular a analizar
    :return: Retorna una lista de texto
    """
    rpta = []
    for texto in datos:
        texto_procesado = re.sub(regex, ' ', texto)
        rpta.append(texto_procesado)

    return rpta


def NormalizarCaracteresEspeciales(datos):
    """
    Normaliza caracteres especiales a standares predefinidos
    :param datos: lista de string, textos a analizar
    :return: lista de string
    """
    rpta = []
    caracteres_especiales = ["[À|à|Á|ã|á|æ|â|Æ]", "[È|É|è|é]", "[Ì|Í|í|ì|î|Î]", "[ñ|Ñ]", "[Ò|Ó|õ|ó|ô|ò|Ô|Õ]", "[ü|ù|û|ú|Ù|Ü|Û|Ú]", "[Ç|ç]"]
    caracteres_standar = ["a", "e", "i", "n", "o", "u", "c"]
    for texto in datos:
        for idx in range(len(caracteres_standar)):
            texto = re.sub(
                caracteres_especiales[idx],
                caracteres_standar[idx],
                texto
            )
        rpta.append(texto)

    return rpta


def NormalizarRisas(datos):
    """
    Normalizar las risas con una tag especial
    :param datos: lista de string, textos a normalizar
    :return: lista de string
    """
    rpta = []
    for texto in datos:
        texto_procesado = re.sub('( | )((J|j)(a|A|e|s|x|E|S|X)+)+', ' RISAS ', texto)
        rpta.append(texto_procesado)

    return rpta
