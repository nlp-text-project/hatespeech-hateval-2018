"""
    Analytic.py

    Contiene todas las funcionalidades para poder analizar la informacion
    de un base de datos de forma visual.

   Buscar responder a las siguientes preguntas:

    - Cuales palabras actuan como stop words ademas del grupo clasico?,
      Algunas son hate speech? (P1)
    - Cuales palabras son más representativas de cada grupo? (P2)
"""
import Utils
