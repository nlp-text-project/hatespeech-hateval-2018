"""
    Process.py

    contiene todos los processo creados para transformar los datos en
    una representación entendible para el modelo a usar
    y poder dar seguimiento de los mecanismos de transformacion
    e hipotesis de la informacion
"""
from collections import defaultdict

import Utils
from math import inf

from Preprocessing import *


def PreprocessingNLP(dir, stopword_dir, limite_inf=0, limite_sup=inf):
    """
    Preprocesa la base de datos

    :param dir: string; direccion de datos
    :param stopword_dir: string, direccion de los archivos de stop words
    :return: retorna la base de datos limpria
    """
    # cargamos los datos y elementos basicos
    data_base = Utils.CargarArchivo(dir, '\t')
    stopwords_list = Utils.CargarStopWords(stopword_dir)
    # segmentar el area de interes
    ####data_base = data_base.loc[data_base['HS'] == 1]
    data_base = data_base.iloc[:, 1:3]
    # convertirlos a una estructura mas apropiada
    data_text = data_base['text'].tolist()
    # proceso de normalizacion de datos
    data_text = ConvertirMinuscula(data_text)
    data_text = NormalizarUsuario(data_text)
    data_text = NormalizarHTTP(data_text)
    data_text = NormalizarHashTag(data_text)
    data_text = NormalizarNumeros(data_text)
    data_text = NormalizarCaracteresEspeciales(data_text)
    data_text = EliminacionUsandoRegex(data_text, '[^ a-zA-Z]+')
    data_text = NormalizarRisas(data_text)
    # eliminar los stopwords
    data_text = EliminarStopWords(data_text, stopwords_list)
    # convertir a una lista de tokens
    data_text = ConvertirListaToken(data_text)

    # PREGUNTA 1: CUALES SON LAS PALABRAS MAS COMUNES EN TODOS
    tabla = Counter()
    cantidad_palabras = 0
    Informacion = ""

    for texto in data_text:
        cantidad_palabras += len(texto)
        for palabra in texto:
            tabla[palabra] += 1

    for dupla in tabla.most_common():
        Informacion += dupla[0] + " = " \
                       + str(float(dupla[1]/cantidad_palabras))\
                       + " (" + str(dupla[1]) + ")\n"

    FileAsk1 = open("Analysis/Pregunta1.txt","w+")
    FileAsk1.write(Informacion)
    FileAsk1.close()

    #PREGUNTA 2: CUALES PALABRAS SON MAS DISTINTIVAS EN CADA GRUPO
    HS = Counter()
    NHS = Counter()
    palabras_corpus = set()

    for idx in range(len(data_text)):
        label = data_base.loc[idx,"HS"]
        for palabras in data_text[idx]:
            palabras_corpus.add(palabras)
            if label == 1:
                HS[palabras] += 1
            else:
                NHS[palabras] += 1

    Informacion = ""
    for palabras in palabras_corpus:
        Informacion += palabras + ":  " + str(NHS[palabras] + HS[palabras]) \
                       + " = " + str(NHS[palabras]) + "(Non-Hate) " + \
                       str(HS[palabras]) + "(Hate)\n"

    FileAsk2 = open("Analysis/Pregunta2.txt", "w+")
    FileAsk2.write(Informacion)
    FileAsk2.close()

    #PREGUNTA 3: CUALES PALABRAS DESAPARECEN REPRESENTATIVAS APARECEN EN CIERTO INTERVALO DADO
    palabras_seleccionadas = Counter()

    Informacion = ""
    cantidad = 0
    for dupla in tabla.most_common():
        if limite_inf <= dupla[1] <= limite_sup:
            cantidad += 1
            Informacion += dupla[0] + ": " + str(NHS[dupla[0]]) + "(Non-Hate) " + \
                           str(HS[dupla[0]])+ "(Hate)\n"

    FileAsk3 = open("Analysis/Pregunta3.txt", "w+")
    FileAsk3.write("Cantidad: " + str(cantidad) + " de "+ str(len(tabla)) + "\n--------------\n")
    FileAsk3.write(Informacion)
    FileAsk3.close()




dev_dir = "Data/Dev/public_development_es_dev_es.tsv"
stopwords_dir = "Data/StopWords/V1.txt"
PreprocessingNLP(dev_dir, stopwords_dir)
