<h1>Hypothesis & Ideas of Project</h1>
<h2>Purpose</h2>
<p>Contain all Hypothesis and ideas to solve this problem</p>
<h2> Content </h2>
<h3>Problems and Challendes (aka P)</h3>
<ol>
    <li>Normalize wrong spelled words. (eg. right: "madre"; wrong: "modre","maaaaaaaadre")</li>
    <li>Find similar Phrase words (eg. right: "puta madre", "re puta re mil madre", "puta de tu madre")</li>
    <li>Find if hate words are stop words</li>
</ol>
<h3>Ideas and Hypothesis to solve</h3>
<ol>
    <li>
        </br>
        <ol>
            <strong><i>Hypothesis (P1)</i></strong>
            <li>There are wrong spelled words by human or technology error.</li>
        </ol>
        <strong>Possible Solution</strong>
        <p>Decompose all words into syllables and create embedding vector of syllables (char2vec)  to make embedding vector of words and finds similiar right spelled word with wrong spelled words </p>
    </li>
    <li>
        </br>
        <ol>
            <strong><i>Hypothesis (P2)</i></strong>
            <li>There are express words in different ways that use many similar words to express the same sense</li>
        </ol>
        <strong>Possible Solution</strong>
        <p>Remove all classic stop words and make N-grams ( N: 2 , 3 and 4) to find phrase nouns and remove useless phrase nouns using a threshold by number of appearance</p>
    </li>
    <li>
        </br>
        <ol>
            <strong><i>Hypothesis (P3)</i></strong>
            <li> If hate words are 50% with margen more or less 10% then those are stop words</li>
        </ol>
        <strong>Possible Solutions</strong>
        <p>Count n-grams by phrase nouns and make a porcentage to check if those are stop words.</p>
    </li>
</ol>