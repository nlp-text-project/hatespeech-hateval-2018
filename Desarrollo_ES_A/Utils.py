"""
    Utils.py

    contiene funciones utilitarias para todos que necesitaran
    para funcionar adecuadamente

    funciones:
        - CargarArchivo
        - CargarStopWords
"""
import pandas as pd


def CargarArchivo(dir_archivo, token):
    """

    :param dir_archivo: String, direccion del archivo
    :param token: String, token separador en el archivo
    :return: Pandas, Archivo cargado en un Objeto pandas
    """
    return pd.read_csv(dir_archivo, sep=token)


def CargarStopWords(dir_archivo):
    """
    Cargar lista de stop words

    :param dir_archivo: string, ubicacion de archivo
    :return: lista de stop words
    """
    rpta = []
    for linea in open(dir_archivo, 'r'):
        rpta.append(linea.split()[0])

    return rpta


